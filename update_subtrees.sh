#!/usr/bin/env bash
git subtree pull -P libs/Big_Easy_Driver --squash git@github.com:sparkfun/Big_Easy_Driver.git master
git subtree pull -P libs/Adafruit-PiTFT-Helper --squash git@github.com:adafruit/Adafruit-PiTFT-Helper.git master
